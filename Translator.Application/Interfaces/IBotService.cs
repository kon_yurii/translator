﻿using Telegram.Bot;

namespace Translator.Application.Interfaces
{
    public interface IBotService
    {
        TelegramBotClient Client { get; }
    }
}
