﻿using System.Threading.Tasks;

namespace Translator.Application.Interfaces
{
    public interface IBotCommandsService
    {
       Task<string> CreateResponse(string innerMessage);
    }
}
