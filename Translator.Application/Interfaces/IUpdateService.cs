﻿using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Translator.Application.Interfaces
{
    public interface IUpdateService
    {
        Task EchoAsync(Update update);
    }
}
