﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Threading.Tasks;
using Translator.Application.Interfaces;
using Translator.Vk.Models;
using VkNet.Abstractions;
using VkNet.Model;
using VkNet.Model.RequestParams;
using VkNet.Utils;

namespace Translator.Vk.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CallbackController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IVkApi _vkApi;
        private readonly IBotCommandsService _commandsService;

        public CallbackController(IVkApi vkApi, IConfiguration configuration, IBotCommandsService commandsService)
        {
            _vkApi = vkApi;
            _configuration = configuration;
            _commandsService = commandsService;
        }

        [HttpPost]
        public async Task<IActionResult> Callback([FromBody] Updates updates)
        {
            switch (updates.Type)
            {
                case "confirmation":
                    return Ok(_configuration["Config:Confirmation"]);
                case "message_new":
                    {
                        var msg = Message.FromJson(new VkResponse(updates.Object));

                        var textResponse = await _commandsService.CreateResponse(msg.Body);

                        _vkApi.Messages.Send(new MessagesSendParams
                        {
                            RandomId = new DateTime().Millisecond,
                            UserId = msg.UserId,
                            Message = msg.Body
                        });
                        break;
                    }
            }
            return Ok("ok");
        }
    }
}